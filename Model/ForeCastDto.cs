﻿using System;

namespace WeatherCore.Model
{
    public class ForeCastDto
    {
        public string CityName { get; set; }
        public double TodaysTemp { get; set; }
        public string SunRise { get; set; }
        public string SunSet { get; set; }
        public double Cloudiness { get; set; }
        public double WindSpeed { get; set; }
        public double SecondDayTemp { get; set; }
        public string SecondDayWeatherCondition { get; set; }
        public string SecondDayWeatherConditionDescription { get; set; }

        public double ThirddayTemp { get; set; }
        public string ThirddayWeatherCondition { get; set; }
        public string ThirddayWeatherConditionDescription { get; set; }
        public double FourthdayTemp { get; set; }
        public string FourthdayWeatherCondition { get; set; }
        public string FourthdayWeatherConditionDescription { get; set; }
        public double FifthdayTemp { get; set; }
        public string FifthdayWeatherCondition { get; set; }
        public string FifthdayWeatherConditionDescription { get; set; }
    }
}