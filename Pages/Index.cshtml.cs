﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using WeatherCore.Model;

namespace WeatherCore.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        [BindProperty] public string Location { get; set; }

        public string LocationResponse { get; set; }
        public ForeCast Weather { get; set; }


        [BindProperty] public string GetIpAddress { get; set; }

        public LocationFinder LocationFinder { get; set; }
        public ForeCastDto ForeCast { get; set; }

        public  void OnGet()
        {


        }

        public JsonResult OnGetWeather()
        {
            //GET IP ADDRESS
            var getIpAddress = new WebClient().DownloadString("https://api.ipify.org");


            //GET THE LOCATION OF THE USER BASE ON HIS IP
            var loc = new WebClient().DownloadString(buildGetIp(getIpAddress));
            //
            //            LocationResponse =await  loc.Content.ReadAsStringAsync();
            //serialize the data
            LocationFinder = JsonConvert.DeserializeObject<LocationFinder>(loc);

            //get the weather base on the location
            var o = new WebClient().DownloadString(buildGetWeather(LocationFinder.city));

            //serialize data
            Weather = JsonConvert.DeserializeObject<ForeCast>(o);

            ForeCast = MapFromWeather(Weather);
            
            return new JsonResult(ForeCast);
        }




        public void OnPost()
        {
            
        }


        public ActionResult OnPostSendLocation()
        {
            string city = string.Empty;
            MemoryStream stream = new MemoryStream();
            Request.Body.CopyTo(stream);
            stream.Position = 0;
            using (StreamReader reader = new StreamReader(stream))
            {
                string requestBody = reader.ReadToEnd();
                if (requestBody.Length > 0)
                {
                    var obj = JsonConvert.DeserializeObject<Location>(requestBody);
                    if (obj != null)
                    {
                        city = obj.City;
                        var o = new WebClient().DownloadString(buildGetWeather(city));
                        //serialize data
                        Weather = JsonConvert.DeserializeObject<ForeCast>(o);

                        ForeCast = MapFromWeather(Weather);

                        return new JsonResult(ForeCast);

                    }
                }
            }

            return new JsonResult(ForeCast);
        }


        private string buildGetWeather(string location)
        {
            return "http://api.openweathermap.org/data/2.5/forecast?q=" + location +"&APPID=bc8864e507de082a27be0930af6601ac";
        }

        private string buildGetIp(string ipaddress)
        {
            return "http://api.ipstack.com/" + ipaddress + "?access_key=afed545198e3d15ca2838516270ac878&&output=json";
        }

        public ForeCastDto MapFromWeather(ForeCast foreCast)
        {
            var foreCastMapping=new ForeCastDto();
            foreCastMapping.CityName = foreCast.city.name;
            var getTemp = foreCast.list.FirstOrDefault(x => DateTime.Parse(x.dt_txt).ToShortDateString() == DateTime.Today.ToShortDateString());
            if (getTemp != null) 
                foreCastMapping.TodaysTemp = KelvinCensius(getTemp.main.temp);
            foreCastMapping.SunRise = GetTimeFromUTC(foreCast.city.sunrise).ToShortTimeString();
            foreCastMapping.SunSet = GetTimeFromUTC(foreCast.city.sunset).ToShortTimeString();
            foreCastMapping.Cloudiness = foreCast.list[0].clouds.all;
            foreCastMapping.WindSpeed = foreCast.list[0].wind.speed;
            var second = foreCast.list.FirstOrDefault(x => DateTime.Parse(x.dt_txt).ToShortDateString() == DateTime.Today.AddDays(1).ToShortDateString());
            if (second != null)
            {
                foreCastMapping.SecondDayTemp = KelvinCensius(second.main.temp);
                foreCastMapping.SecondDayWeatherCondition = second.weather[0].main.ToLower();
                foreCastMapping.SecondDayWeatherConditionDescription = UppercaseWords( second.weather[0].description);
            } 
            
            var third = foreCast.list.FirstOrDefault(x => DateTime.Parse(x.dt_txt).ToShortDateString() == DateTime.Today.AddDays(2).ToShortDateString());
            if (third != null)
            {
                foreCastMapping.ThirddayTemp = KelvinCensius(third.main.temp);
                foreCastMapping.ThirddayWeatherCondition = third.weather[0].main.ToLower();
                foreCastMapping.ThirddayWeatherConditionDescription = UppercaseWords(third.weather[0].description);
            }            
            
            var fourth = foreCast.list.FirstOrDefault(x => DateTime.Parse(x.dt_txt).ToShortDateString() == DateTime.Today.AddDays(3).ToShortDateString());
            if (fourth != null)
            {
                foreCastMapping.FourthdayTemp = KelvinCensius( fourth.main.temp);
                foreCastMapping.FourthdayWeatherCondition = fourth.weather[0].main.ToLower();
                foreCastMapping.FourthdayWeatherConditionDescription = UppercaseWords( fourth.weather[0].description);
            }
            
            var fifth = foreCast.list.FirstOrDefault(x => DateTime.Parse(x.dt_txt).ToShortDateString() == DateTime.Today.AddDays(4).ToShortDateString());
            if (fifth != null)
            {
                foreCastMapping.FifthdayTemp = KelvinCensius( fifth.main.temp);
                foreCastMapping.FifthdayWeatherCondition = fifth.weather[0].main.ToLower();
                foreCastMapping.FifthdayWeatherConditionDescription = UppercaseWords(fifth.weather[0].description);
            }

            return foreCastMapping;
        }

   
        public DateTime GetTimeFromUTC(long time)
        {
            // ReSharper disable once SuggestVarOrType_SimpleTypes
            TimeZone localZone = TimeZone.CurrentTimeZone;
            TimeZoneInfo cstZone = TimeZoneInfo.Local;
            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds(time);
            return TimeZoneInfo.ConvertTimeFromUtc(dateTimeOffset.DateTime, cstZone);
        }

        private static double KelvinCensius(double d)
        {
                return d - 273.15;
        }

        static string UppercaseWords(string value)
        {
            char[] array = value.ToCharArray();
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
          
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
    }

    public class Location
    {
        public string City { get; set; }
    }
}